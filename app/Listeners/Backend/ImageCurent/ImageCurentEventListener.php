<?php

namespace App\Listeners\Backend\ImageCurent;

/**
 * Class ImageCurentEventListener.
 */
class ImageCurentEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->image_curent->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->image_curent->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->image_curent->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\ImageCurent\ImageCurentCreated::class,
            'App\Listeners\Backend\ImageCurent\ImageCurentEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\ImageCurent\ImageCurentUpdated::class,
            'App\Listeners\Backend\ImageCurent\ImageCurentEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\ImageCurent\ImageCurentDeleted::class,
            'App\Listeners\Backend\ImageCurent\ImageCurentEventListener@onDeleted'
        );
    }
}
