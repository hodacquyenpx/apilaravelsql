<?php

namespace App\Repositories\Backend;

use App\Models\Image;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ImageRepository.
 */
class ImageRepository extends BaseRepository
{
    public function __construct(Image $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return Image
     */
    public function create(array $data) : Image
    {
        return DB::transaction(function () use ($data) {
            $image = $this->model::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'image' => $data['image'],
            ]);

            if ($image) {
                return $image;
            }

            throw new GeneralException("Upload Fail!");
        });
    }
}
