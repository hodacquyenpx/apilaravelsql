<?php

namespace App\Events\Backend\ImageCurent;

use Illuminate\Queue\SerializesModels;

/**
 * Class ImageCurentUpdated.
 */
class ImageCurentUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $image_curents;

    /**
     * @param $image_curents
     */
    public function __construct($image_curents)
    {
        $this->image_curents = $image_curents;
    }
}
